<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Select2Example extends Component
{
    public function render()
    {
        return view('livewire.select2-example');
    }
}
