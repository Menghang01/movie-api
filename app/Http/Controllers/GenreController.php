<?php

namespace App\Http\Controllers;
use App\Models\Genre; 
use Illuminate\Http\Request;

class GenreController extends Controller
{
    public function store(Request $request){
        $genre = Genre::create([
            "name" => $request->name
        ]); 
        return response($genre); 
    }

    public function index(){
        $genres = Genre::with(['movies'])->get();
        return view("/genre", ["genres" => $genres]); 
    }
}
