<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\RedirectResponse;
use App\Models\Movie;
use App\Models\Genre; 


class MovieController extends Controller
{

    public function index(){
      
        $movies = Movie::with(['genres','director'])->get(); 
        $genres = Genre::all(); 
        return view("/welcome", ['movies' => $movies, 'genres' => $genres]);  
    }


    public function store(Request $request){
        $validated = $request->validate([
            "title" => 'required|string', 
            "description" => 'required|string', 
        ]); 

        $movie = new Movie(); 
        $movie->fill($validated); 
        $movie->director()->associate($request->director_id); 
        $movie->save(); 
        $movie->genres()->attach($request->genres); 

        $movies = Movie::all(); 
        $genres = Genre::all(); 

        return view("/welcome", ['movies' => $movies, 'genres' => $genres]);

    }


    public function edit(Request $request){
        $movie = Movie::find($request->id);
        $movie->title = $request->title; 
        $movie->description  = $request->description; 
        $movie->director_id = $request->director_id; 
        
        $movie->save(); 

        $movie->genres()->attach($request->genres); 
        return response("Success", 201);
    }

    public function delete($id){
        $movie = Movie::find($id); 
        $movie->delete();
        return response("success", 204);
    }
}
