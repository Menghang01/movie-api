<?php

namespace App\Http\Controllers;
use App\Models\Director; 
use Illuminate\Http\Request;

class DirectorController extends Controller
{
    public function store(Request $request){
        $director = Director::create([
            'name' => $request->name
        ]); 
        return response($director); 
    }

    public function index(){
        $directors = Director::with(["movies"])->get(); 
        error_log($directors);
        return view("/director", ["directors" => $directors]);
    }
}
