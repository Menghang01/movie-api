<!DOCTYPE html>
<html>
<head>
<title>Datatables implementation in laravel - justlaravel.com</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<script src="//code.jquery.com/jquery-1.12.3.js"></script>
<script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/1.1.1/js/bootstrap-multiselect.min.js" integrity="sha512-fp+kGodOXYBIPyIXInWgdH2vTMiOfbLC9YqwEHslkUxc8JLI7eBL2UQ8/HbB5YehvynU3gA3klc84rAQcTQvXA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script
	src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>

	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

	
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.css">
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

   <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
  


 

</head>
<style>
</style>
<body>
	
<div class="container table-responsive py-5"> 
<div style="margin-bottom:36px">
		<button class=" btn btn-info">
			<a href="/genre">
			<span class=""></span> View All Genre


			</a>
		</button>
		<button class="btn btn-info">
			<a href="/director" class="link-light">
			<span class="text-white"></span> View All Directors
			</a>
		</button>

    <button class="btn btn-info">
			<a href="/" class="link-light">
			<span class="text-white"></span> View All Movies
			</a>
		</button>
	
			
		</div>
<table class="table table-bordered table-hover">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Name</th>
      <th scope="col">Movies</th>
      <th scope="col">Last Updated</th>

    </tr>
  </thead>
  <tbody>

    
    @foreach($genres as $genre)

    <tr>
      <th scope="row">{{$genre->id}}</th>
      <td>{{$genre->name}}</td>
      <td>
          @if(count($genre->movies) > 0)
          @foreach($genre->movies as $movie)
           <span>{{$movie->title . ","}}</span>
           @endforeach

          @else 
          <span>No movies for this genre yet</span>
          @endif
      </td>
      <td>{{$genre->updated_at}}</td>
    </tr>

    @endforeach
   
  </tbody>
</table>
</div>








</body>
</html>