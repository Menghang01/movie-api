<!DOCTYPE html>
<html>
<head>
<title>Datatables implementation in laravel - justlaravel.com</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<script src="//code.jquery.com/jquery-1.12.3.js"></script>
<script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/1.1.1/js/bootstrap-multiselect.min.js" integrity="sha512-fp+kGodOXYBIPyIXInWgdH2vTMiOfbLC9YqwEHslkUxc8JLI7eBL2UQ8/HbB5YehvynU3gA3klc84rAQcTQvXA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script
	src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>

	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

	
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.css">
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

   <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
  


 

</head>
<style>
</style>
<body>
	<div class="container ">
		{{ csrf_field() }}
		<div style="margin-bottom:36px">
		<button class="btn btn-info">

			<a href="/genre">
			<span class=""></span> View All Genre


			</a>
		</button>
		<button class="btn btn-info">
			<a href="/director" class="link-light">
			<span class="text-white"></span> View All Directors
			</a>
		</button>
		<button class="create-modal btn btn-info">
			<span class="glyphicon glyphicon-edit"></span> Create
		</button>

		
			
		</div>
		<div class="table-responsive text-center">
			
		
			<table class="table table-borderless" id="table">
				<thead>
					<tr>
						<th class="text-center">#</th>
						<th class="text-center">Movie title</th>
						<th class="text-center">Description</th>
						<th class="text-center">Genres</th>
						<th class="text-center">Director_id</th>
						<th class="text-center"></th>
						<th class="text-center">Actions</th>
						<th class="text-center"></th>
					</tr>
				</thead>
				@foreach($movies as $item)
				<tr class="item{{$item->id}}">
					<td>{{$item->id}}</td>
					<td>{{$item->title}}</td>
					<td>{{substr($item->description, 0,50)}}</td>
					<td>
					
						@foreach($item->genres as $genre)
							<span>{{$genre->name}}</span>
						@endforeach
					
					</td>
					<td>{{$item->director->name}}</td>
					<td>{{$item->salary}}</td>
					<td><button class="edit-modal btn btn-info"
							data-info="{{$item->id}}~{{$item->title}}~{{$item->description}}~{{$item->director->name}}~{{$item->director->id}}~{{$item->genres}}">
							<span class="glyphicon glyphicon-edit"></span> Edit
						</button>
						<button class="delete-modal btn btn-danger"
							data-info="{{$item->id}}~{{$item->first_name}}~{{$item->last_name}}~{{$item->director_id}}~{{$item->genres}}">
							<span class="glyphicon glyphicon-trash"></span> Delete
						</button></td>
				</tr>
				@endforeach
			</table>
		</div>
	</div>
	<div id="myModal" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"></h4>

				</div>
				<div class="modal-body">
					<form class="form-horizontal" role="form">
						<div class="form-group">
							<label class="control-label col-sm-2" for="id">ID</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="fid" disabled>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-2" for="title">Movie Title</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="title">
							</div>
						</div>
						<p class="title_error error text-center alert alert-danger hidden"></p>
						<div class="form-group">
							<label class="control-label col-sm-2" for="description">Description</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="description">
							</div>
						</div>
						<p class="description_error error text-center alert alert-danger hidden"></p>
						<div class="form-group">
							<label class="control-label col-sm-2" for="director_id">Director</label>
							<div class="col-sm-10">
								<input type="director_id" class="form-control" id="director_id">
								<input type="number" id="director_id" style="display:none" >
							</div>
						</div>
						<p class="director_id_error error text-center alert alert-danger hidden"></p>
						<div class="form-group">
							<label class="control-label col-sm-2" for="genres">Genres</label>
							<div class="col-sm-10">
								<select multiple class="form-control selectpicker" data-live-search="true"  id="genres" name="genres[]">
									@foreach($genres as $genre)
									<option 

									value={{$genre->id}} 
										>{{$genre->name}}</option>
									@endforeach
  							  </select>
							</div>			
						</div>		
					</form>
					
					<div class="deleteContent">
						Are you Sure you want to delete <span class="dname"></span> ? <span
							class="hidden did"></span>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn actionBtn" data-dismiss="modal">
							<span id="footer_action_button" class='glyphicon'> </span>
						</button>
						<button type="button" class="btn btn-warning closex" data-dismiss="modal">
							<span class='glyphicon glyphicon-remove'></span> Close
						</button>
					</div>




					 

					






				</div>
			</div>
		</div>
	</div>





	<script>
  $(document).ready(function() {
    $('#table').DataTable();

} );
  </script>

	<script>
		
	$(document).on('click', '.create-modal', function() {
        $('#footer_action_button').text(" Create");
        $('#footer_action_button').addClass('glyphicon-check');
        $('#footer_action_button').removeClass('glyphicon-trash');
        $('.actionBtn').addClass('btn-success');
        $('.actionBtn').removeClass('btn-danger');
        $('.actionBtn').removeClass('delete');
        $('.actionBtn').addClass('create');
        $('.modal-title').text('Create');
        $('.deleteContent').hide();
        $('.form-horizontal').show();


        $('#myModal').modal('show');
    });


	$(document).on('click', '.closex', function() {
      deleteModalDat()
    });
	

    $(document).on('click', '.edit-modal', function() {
        $('#footer_action_button').text(" Update");
        $('#footer_action_button').addClass('glyphicon-check');
        $('#footer_action_button').removeClass('glyphicon-trash');
        $('.actionBtn').addClass('btn-success');
        $('.actionBtn').removeClass('btn-danger');
        $('.actionBtn').removeClass('delete');
        $('.actionBtn').addClass('edit');
        $('.modal-title').text('Edit');
        $('.deleteContent').hide();
		$('.form-horizontal2').hide();

        $('.form-horizontal').show();
        var stuff = $(this).data('info').split('~');
        fillmodalData(stuff)
        $('#myModal').modal('show');
    });

    $(document).on('click', '.delete-modal', function() {
        $('#footer_action_button').text(" Delete");
        $('#footer_action_button').removeClass('glyphicon-check');
        $('#footer_action_button').addClass('glyphicon-trash');
        $('.actionBtn').removeClass('btn-success');
        $('.actionBtn').addClass('btn-danger');
        $('.actionBtn').removeClass('edit');
        $('.actionBtn').addClass('delete');
        $('.modal-title').text('Delete');
        $('.deleteContent').show();
        $('.form-horizontal').hide();
		$('.form-horizontal2').hide();
		$('.form-create').hide();

        var stuff = $(this).data('info').split('~');
		console.log(stuff);
        $('.did').text(stuff[0]);
        $('.dname').html(stuff[1] +" "+stuff[2]);
        $('#myModal').modal('show');
    });

function fillmodalData(details){
    $('#fid').val(details[0]);
    $('#title').val(details[1]);
    $('#description').val(details[2]);
    $('#director').val(details[3]);
	$('#director_id').val(details[4]); 
    $('#genres').val(details[5]);

}

function deleteModalDat(){
	$('#fid').val("");
    $('#title').val("");
    $('#description').val("");
    $('#director').val("");
	$('#director_id').val(""); 
    $('#genres').val("");
}


    $('.modal-footer').on('click', '.edit', function() {
	
        $.ajax({
            type: 'put',
            url: '/movie' ,
            data: {
                '_token': $('input[name=_token]').val(),
                'id': $("#fid").val(),
                'title': $('#title').val(),
                'description': $('#description').val(),
                'director_id': $('#director_id').val(),
                'genres': $('#genres').val(),
              
            },
            success: function(data) {
            	if (data.errors){
                	$('#myModal').modal('show');
                    if(data.errors.title) {
                    	$('.title_error').removeClass('hidden');
                        $('.title_error').text("First name can't be empty !");
                    }
                    if(data.errors.description) {
                    	$('.description_error').removeClass('hidden');
                        $('.description_error').text("Last name can't be empty !");
                    }
                    if(data.errors.director_id) {
                    	$('.director_id_error').removeClass('hidden');
                        $('.director_id_error').text("director must be a valid one !");
                    }
                  
                }
            	 else {
                     $('.error').addClass('hidden');
                $('.item' + data.id).replaceWith("<tr class='item" + data.id + "'><td>" +
                        data.id + "</td><td>" + data.first_name +
                        "</td><td>" + data.last_name + "</td><td>" + data.director_id + "</td><td>" +
                         data.genres + "</td><td>" + data.country + "</td><td>" + data.salary +
                          "</td><td><button class='edit-modal btn btn-info' data-info='" +
						   data.id+","+data.first_name+","+data.last_name+","+data.director_id+","
						   +data.genres+","+data.country+","+data.salary+
						   "'><span class='glyphicon glyphicon-edit'></span> Edit</button> <button class='delete-modal btn btn-danger' data-info='" 
						   + data.id+","+data.first_name+","+data.last_name+","+data.director+","+data.genres+","+data.country+","+data.salary
						   +"' ><span class='glyphicon glyphicon-trash'></span> Delete</button></td></tr>")
					
            	 }}
        });
    });
    $('.modal-footer').on('click', '.delete', function() {
		var a =  $('.did').text();
		console.log(a);
        $.ajax({
            type: 'delete' ,
            url: '/movie/' + a,
            data: {
                '_token': $('input[name=_token]').val(),
            },
            success: function(data) {
                $('.item' + $('.did').text()).remove();
				alert("success");
            }
        });
    });

	$('.modal-footer').on('click', '.create', function() {
        $.ajax({
            type: 'post',
            url: '/movie',
            data: {
				'_token': $('input[name=_token]').val(),
                'title': $('#title').val(),
                'description': $('#description').val(),
                'director_id': $('#director_id').val(),
                'genres': $('#genres').val(),
              
            },
            success: function(data) {
                deleteModalDat();
				alert("success")
				location.reload();

            }
        });
    });
</script>

</body>
</html>