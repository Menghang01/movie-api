<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MovieController; 
use App\Http\Controllers\DirectorController; 
use App\Http\Controllers\GenreController; 


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes cfor your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [MovieController::class, 'index']);
Route::put('/movie', [MovieController::class, 'edit']);
Route::post('/movie', [MovieController::class, 'store']);
Route::delete('/movie/{id}', [MovieController::class, 'delete']);

Route::get("/genre", [GenreController::class, 'index'] );
Route::get("/director", [DirectorController::class, 'index'] );



Route::get("/test", function(){
    return view('test');
}); 

