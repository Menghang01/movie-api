<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MovieController; 
use App\Http\Controllers\DirectorController; 
use App\Http\Controllers\GenreController; 


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post("/movie", [MovieController::class, 'store']); 
Route::post("/director", [DirectorController::class, 'store']);
Route::post("/genre", [GenreController::class, 'store']);
Route::get('/movie', [MovieController::class, 'index']); 

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
